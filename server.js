const express = require('express');

const connectDB = require('./config/db');

const app = express();

const PORT = process.env.PORT || 5000;

//Mongo DB Connection
connectDB();

//Init middles here
app.use(express.json({ extended: false }));

app.get('/', (req, res) => {
    res.json({
        msg: 'Welcome to our Contact keeper application'
    })
})

app.use('/api/users', require('./routes/users'));
app.use('/api/products', require('./routes/products'));
app.use('/api/auth', require('./routes/auth'));


app.listen(PORT, () => console.log(`Server started on port ${PORT}`));