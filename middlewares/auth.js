const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req, res, next) {
    //Get token from header

    var token = req.header('x-auth-token');

    //Check if token exists
    if (!token) {
        return res.status(401).json({
            msg: 'No token, Autherization denied'
        })
    }

    try {
        const decode = jwt.verify(token, config.get('jwtSecret'));
        req.user = decode.user;
        next();
    } catch (err) {
        res.status(401).json({ msg: 'Token is invalid' })
    }
}