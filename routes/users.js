const express = require('express');
const router = express.Router();
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const config = require('config');
const { check, validationResult } = require('express-validator/check');
const bcrypt = require('bcryptjs');

//@route    POST    /api/users
//@desc     Register users into application
//@access   Public
router.post('/',
    [
        check('name', 'Please enter name').not().isEmpty(),
        check('email', 'Please enter valid email').isEmail(),
        check('password', 'Please enter password with 6 or more characters').isLength({
            min: 6
        })
    ],

    async (req, res) => {
        var errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array()
            })
        }

        try {
            const { name, email, password } = req.body;
            var user = await User.findOne({email});
            if(user){
                return res.status(400).json({
                    msg: 'User already exists'
                })
            }

            user = new User({
                name,
                email,
                password
            })

            const salt = await bcrypt.genSalt(10);
            user.password = await bcrypt.hash(password, salt);

            await user.save();
            const payload = {
                user: {
                    id: user.id
                }
            }

            jwt.sign(payload, config.get('jwtSecret'),{
                expiresIn: 36000
            }, (err, token)=>{
                if(err) throw err;

                res.json({
                    token
                })
            })

        }
        catch (err) {
            console.log(err.message);
            res.status(500).send('Server error');
        }

    })

module.exports = router;