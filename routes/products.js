const express = require('express');
const router = express.Router();
const User = require('../models/User');
const { check, validationResult } = require('express-validator/check');
const auth = require('../middlewares/auth');
const Product = require('../models/Product');
const Cart = require('../models/Cart');


router.get('/', auth, async (req, res) => {
    try {

        const products = await Product.find({ user: req.user.id }).sort({ date: -1 });
        res.json(products);

    } catch (err) {
        console.log(err.message);
        res.status(500).json({ msg: 'Server error' })

    }

})

router.post('/', [auth, [
    check('name', 'Name is required').not().isEmpty(),
    check('description', 'Description is required').not().isEmpty(),
    check('price', 'Price is required').not().isEmpty(),
    check('price', 'Price is required').isNumeric(),
    check('make', 'Price is required').not().isEmpty(),
    check('make', 'Price is required').isNumeric(),
]], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const { name, description, price, make } = req.body;
    try {
        const newProduct = new Product({
            name,
            description,
            price,
            make,
            user: req.user.id
        })

        const product = await newProduct.save();
        res.json(product);

    } catch (err) {
        console.log(err.message);
        res.status(500).json({ msg: 'Server error' })

    }
})


router.put('/:id',
    auth,
    async (req, res) => {
        const { name, description, price, make } = req.body;

        try {
            let product = await Product.findById(req.params.id);
            if (!product) return res.status(404).json({ msg: 'Product not found' })

            if (contact.user.toString() !== req.user.id) return res.status(401).json({ msg: 'Not autherized' });

            let productInfo = {};
            if (name) productInfo.name = name;
            if (description) productInfo.description = description;
            if (price) productInfo.price = price;
            if (make) productInfo.make = make;

            product = await Product.findByIdAndUpdate(req.params.id, {
                $set: productInfo
            }, { new: true })

            res.json(product);
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ msg: 'Server error' })
        }
    })


router.delete('/:id', auth, async (req, res) => {
    try {
        let product = await Product.findById(req.params.id);
        if (!product) return res.status(404).json({ msg: 'Product not found' })

        if (product.user.toString() !== req.user.id) return res.status(401).json({ msg: 'Not autherized' });

        await Product.findByIdAndRemove(req.params.id)

        res.json({ msg: 'Product removed' });
    } catch (err) {
        console.log(err.message);
        res.status(500).json({ msg: 'Server error' })
    }
});

router.post('/addtocart', [
    auth,
    [
        check('products', 'Product is required').exists(),
        check('products', 'Product is required').isArray()
    ]
], async (req, res) => {
    const { products } = req.body;

    const user = req.user;
    try {

        const newCart = new Cart({
            product: products,
            user: user.id
        })

        let cart = await newCart.save();
        return res.json({
            msg: 'Success',
            cart
        })

    } catch (e) {
        console.log(e.message);
        res.status(500).json({ msg: 'Server error' })
    }
})

router.get('/cart', auth, async (req, res) => {

    try {

        const user = req.user;
        const userCarts = await Cart.find({ user: user.id, is_deleted: false })
        .populate('product')

        return res.json(userCarts);
    } catch (e) {
        console.log(e.message);
        res.status(500).json({ msg: 'Server error' })
    }
})

router.get('/all', async(req, res) => {
    try {
        const products = await Product.find({});
        return res.json(products);
    } catch (e) {
        console.log(e.message);
        res.status(500).json({ msg: 'Server error' })
    }
})


module.exports = router;